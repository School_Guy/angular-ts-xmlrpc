/*
 * Public API Surface of angular-xmlrpc
 */

export * from './lib/angular-xmlrpc.service';
export * from './lib/angular-xmlrpc.component';
export * from './lib/angular-xmlrpc.module';
