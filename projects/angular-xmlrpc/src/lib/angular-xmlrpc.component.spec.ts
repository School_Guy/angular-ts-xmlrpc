import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AngularXmlrpcComponent } from './angular-xmlrpc.component';

describe('AngularXmlrpcComponent', () => {
  let component: AngularXmlrpcComponent;
  let fixture: ComponentFixture<AngularXmlrpcComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AngularXmlrpcComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AngularXmlrpcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
