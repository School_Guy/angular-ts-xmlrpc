import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {serializeMethodCall} from './serializer';
import {MethodFault, MethodResponse} from '../typings';
import {deserialize} from './deserializer';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export const applicationError: MethodFault = {faultCode: -1, faultString: 'Problem while processing data.'};

@Injectable({
  providedIn: 'root'
})
export class AngularXmlrpcService {
  private readonly http: HttpClient;
  private url: URL;
  private headers: any;
  private headersProcessors: any;

  constructor(http: HttpClient) {
    // Set the HTTP request headers
    this.headers = {};
    this.url = new URL('http://localhost');
    this.http = http;

    this.headersProcessors = {
      processors: [],
      composeRequest(requestHeaders: any): void {
        this.processors.forEach((p: any) => {
          p.composeRequest(requestHeaders);
        });
      },
      parseResponse(requestHeaders: any): void {
        this.processors.forEach((p: any) => {
          p.parseResponse(requestHeaders);
        });
      }
    };
  }

  /**
   * Call this method before any other to configure the service. Otherwise other methods may behave incorrectly.
   *
   * @param url The URL of the XMLRPC Service.
   */
  configureService(url: URL): void {
    this.url = url;
  }

  /**
   * Makes an XML-RPC call to the server specified by the constructor's options.
   *
   * @param method The method name.
   * @param params Params to send in the call.
   * @param encoding The encoding to append to the generated XML document.
   */
  methodCall(method: string, params?: Array<any>, encoding?: string): Observable<MethodResponse | MethodFault> {
    const xml = serializeMethodCall(method, params, encoding);
    const httpOptions = {
      'User-Agent': 'Angular XML-RPC Client',
      'Content-Type': 'text/xml',
      Accept: 'text/xml',
      'Accept-Charset': 'UTF8',
      Connection: 'Keep-Alive'
    };

    const finalUrl = new URL(this.url.toString() + '/' + method);
    return this.http.post<string>(finalUrl.toString(), xml, {headers: httpOptions}).pipe(
      map<string, MethodResponse | MethodFault>((source) => {
        return deserialize(source);
      })
    );
  }
}
