import { NgModule } from '@angular/core';
import { AngularXmlrpcComponent } from './angular-xmlrpc.component';



@NgModule({
  declarations: [AngularXmlrpcComponent],
  imports: [
  ],
  exports: [AngularXmlrpcComponent]
})
export class AngularXmlrpcModule { }
