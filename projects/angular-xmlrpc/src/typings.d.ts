export type XmlRpcValue = number | boolean | string | Date | Buffer | Struct;
export type MethodResponse = Param;

export interface MethodFault {
  faultCode: number;
  faultString: string;
}

export interface Struct {
  members: Array<Member>;
}

export interface Member {
  name: string;
  value: XmlRpcValue;
}

export interface Param {
  value: XmlRpcValue;
}
